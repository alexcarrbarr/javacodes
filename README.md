# javacodes
Repositorio en el que iré subiendo programas hechos en Java para su uso libre por otros desarrolladores.

* Instrucciones generales de instalación y ejecución de estos proyectos:
1. Descargar proyecto desde este repositorio a su equipo local.
2. Abrir proyecto descargado con algún IDE como, por ej., Eclipse.
3. Hacer click derecho sobre el proyecto y elegir opción para Ejecutar Aplicación Java.
4. Ir a la Consola del IDE y utilizar el programa cargado.

Cualquier consulta, comentario, crítica o aporte que quieran realizar pueden hacerlo
publicando un issue en este repo, o a través de mis RRSS:
Fb: /alexcarrascob
Tw: @alexcarrascob
G+: AlexCarrascoB
Li: /alexcarrascob
GH: /alexcarrascob
